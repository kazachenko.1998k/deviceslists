import androidx.compose.animation.AnimatedContent
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.darkColors
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Window
import androidx.compose.ui.window.application
import com.russhwolf.settings.Settings
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import sceens.LoginScreen
import kotlin.time.Duration.Companion.seconds


val DesignBackground = Color(0xFF08080A)
val DesignPrimary = Color(0xFF645AE4)
val DesignPrimaryLight = Color(0xFF9B87FF)
val DesignRed = Color(0xFFDB5857)
val DesignSecondary = Color(0xFFFCB036)
val DesignSurface = Color(0xFF1F1F24)
val DesignTextPrimary = Color(0xFFFFFFFF)
val DesignTextDescription = Color(0xFFB0B7BF)

val darkPrimary = DesignPrimary
val darkPrimaryVariant = DesignPrimaryLight
val darkSecondary = DesignSecondary
val darkSecondaryVariant = DesignSecondary
val darkBackground = DesignBackground
val darkSurface = DesignSurface
val darkError = DesignRed
val darkOnPrimary = DesignTextPrimary
val darkOnSecondary = DesignTextDescription
val darkOnBackground = DesignTextPrimary
val darkOnSurface = DesignTextPrimary
val darkOnError = DesignTextPrimary

val sideEffect = MutableSharedFlow<String>()
val currentScreen = MutableStateFlow<Screen?>(null)

interface Screen {
    @Composable
    fun draw()
}

val preferencesSettings by lazy { Settings() }

@Composable
fun App() {
    val colors = darkColors(
        primary = darkPrimary,
        primaryVariant = darkPrimaryVariant,
        secondary = darkSecondary,
        secondaryVariant = darkSecondaryVariant,
        background = darkBackground,
        surface = darkSurface,
        error = darkError,
        onPrimary = darkOnPrimary,
        onSecondary = darkOnSecondary,
        onBackground = darkOnBackground,
        onSurface = darkOnSurface,
        onError = darkOnError,
    )
    var effect by remember { mutableStateOf<String?>(null) }
    val screen by currentScreen.collectAsState()
    LaunchedEffect(sideEffect) {
        sideEffect.collect {
            effect = it
            delay(2.seconds)
            effect = null
        }
    }
    MaterialTheme(colors) {
        Box(
            modifier = Modifier
                .fillMaxSize()
                .background(MaterialTheme.colors.background)
        ) {
            AnimatedContent(screen) {
                it?.draw()
            }
            AnimatedContent(effect) {
                if (it == null) Unit else {
                    Text(
                        text = it,
                        color = MaterialTheme.colors.onSurface,
                        style = MaterialTheme.typography.body1,
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(16.dp)
                            .clip(MaterialTheme.shapes.medium)
                            .background(MaterialTheme.colors.surface)
                            .padding(16.dp)
                    )
                }
            }
        }
    }
}

fun main() {
    currentScreen.value = LoginScreen
    application {
        Window(
            title = "Instrumentation",
            onCloseRequest = ::exitApplication
        ) {
            App()
        }
    }
}

