package sceens

import Screen
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import createTablesIfNotExists
import elements.BackToolbar
import elements.UserInputText
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import model.Participation
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.transactions.transaction
import sideEffect

object ParticipationScreen : Screen {
    @Composable
    override fun draw() {
        View()
    }
}


@OptIn(ExperimentalFoundationApi::class)
@Composable
private fun View() {
    var items by remember {
        mutableStateOf<List<ResultRow>>(emptyList())
    }
    var changes by remember {
        mutableStateOf(0)
    }
    var nameEdit by remember { mutableStateOf("") }
    val scope = rememberCoroutineScope()

    LaunchedEffect(changes) {
        withContext(Dispatchers.IO) {
            transaction {
                createTablesIfNotExists(
                    Participation
                )
                val table = Participation
                    .selectAll()
                    .orderBy(Participation.id, order = SortOrder.DESC)
                    .toList()
                items = table
            }
        }
    }
    Column(
        modifier = Modifier.fillMaxWidth(), horizontalAlignment = Alignment.CenterHorizontally
    ) {
        BackToolbar("Участие в АЗ")
        LazyColumn(
            modifier = Modifier.weight(1f).fillMaxWidth()
        ) {
            stickyHeader {
                Row(
                    modifier = Modifier
                        .background(MaterialTheme.colors.background)
                        .padding(
                            horizontal = 16.dp,
                            vertical = 4.dp
                        ),
                    horizontalArrangement = Arrangement.spacedBy(8.dp),
                    verticalAlignment = Alignment.CenterVertically,
                ) {
                    Text(
                        modifier = Modifier.weight(0.25f),
                        color = MaterialTheme.colors.onBackground,
                        text = "Id",
                    )
                    Text(
                        modifier = Modifier.weight(1f),
                        color = MaterialTheme.colors.onBackground,
                        text = "Name",
                    )
                    Text(
                        modifier = Modifier.minimumInteractiveComponentSize(),
                        color = MaterialTheme.colors.onBackground,
                        text = "Delete",
                    )
                }
            }
            itemsIndexed(items,
                key = { _, item -> item[Participation.id] }
            ) { index, result ->
                Row(
                    modifier = Modifier
                        .animateItemPlacement()
                        .background(
                            if (index % 2 == 0) {
                                MaterialTheme.colors.surface.copy(alpha = 0.3f)
                            } else {
                                MaterialTheme.colors.background
                            }
                        )
                        .padding(
                            horizontal = 16.dp,
                            vertical = 8.dp
                        ),
                    horizontalArrangement = Arrangement.spacedBy(8.dp),
                    verticalAlignment = Alignment.CenterVertically,
                ) {
                    Text(
                        modifier = Modifier.weight(0.25f),
                        color = MaterialTheme.colors.onBackground,
                        text = result[Participation.id].toString(),
                    )
                    Text(
                        modifier = Modifier.weight(1f),
                        color = MaterialTheme.colors.onBackground,
                        text = result[Participation.name],
                    )
                    IconButton(onClick = {
                        scope.launch(Dispatchers.IO) {
                            runCatching {
                                transaction {
                                    Participation.deleteWhere {
                                        id eq result[Participation.id]
                                    }
                                }
                            }.onFailure {
                                sideEffect.emit(it.localizedMessage)
                            }.onSuccess {
                                changes++
                            }
                        }
                    }) {
                        Icon(
                            painter = painterResource("exit.svg"),
                            contentDescription = "Delete",
                            tint = MaterialTheme.colors.error,
                        )
                    }
                }
            }
        }
        Row(
            modifier = Modifier.padding(16.dp).fillMaxWidth(), horizontalArrangement = Arrangement.spacedBy(12.dp)
        ) {
            UserInputText(
                modifier = Modifier.weight(0.25f),
                header = "Id",
                textFieldValue = "Auto",
                onTextChanged = {}
            )
            UserInputText(
                modifier = Modifier.weight(1f),
                header = "Name",
                textFieldValue = nameEdit,
                onTextChanged = { nameEdit = it }
            )
        }
        Button(
            modifier = Modifier
                .padding(bottom = 16.dp)
                .padding(horizontal = 16.dp)
                .fillMaxWidth()
                .height(48.dp),
            contentPadding = PaddingValues(horizontal = 16.dp),
            enabled = nameEdit.isNotEmpty(),
            onClick = {
                scope.launch(Dispatchers.IO) {
                    runCatching {
                        transaction {
                            Participation.insert { statement ->
                                statement[name] = nameEdit
                            }
                        }
                    }.onFailure {
                        sideEffect.emit(it.localizedMessage)
                    }.onSuccess {
                        nameEdit = ""
                        changes++
                    }
                }
            },
        ) {
            Text(
                text = "Add", color = MaterialTheme.colors.onPrimary
            )
        }
    }
}
