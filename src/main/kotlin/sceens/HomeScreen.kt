package sceens

import Const.baseUrlKey
import Const.loginKey
import Const.passwordKey
import Screen
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import currentScreen
import elements.NavigationButton
import preferencesSettings

object HomeScreen : Screen {
    @Composable
    override fun draw() {
        View()
    }

}

@Composable
private fun View() {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .verticalScroll(rememberScrollState()),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(
            modifier = Modifier
                .padding(top = 24.dp)
                .padding(horizontal = 16.dp),
            text = "Выберите таблицу",
            color = MaterialTheme.colors.onBackground,
            style = MaterialTheme.typography.h4
        )
        NavigationButton(
            modifier = Modifier
                .padding(top = 16.dp)
                .padding(horizontal = 16.dp)
                .fillMaxWidth()
                .height(48.dp),
            header = "Приборы",
            screen = DevicesScreen
        )
        NavigationButton(
            modifier = Modifier
                .padding(top = 16.dp)
                .padding(horizontal = 16.dp)
                .fillMaxWidth()
                .height(48.dp),
            header = "Участие в АЗ",
            screen = ParticipationScreen
        )
        NavigationButton(
            modifier = Modifier
                .padding(top = 16.dp)
                .padding(horizontal = 16.dp)
                .fillMaxWidth()
                .height(48.dp),
            header = "Канал алгоритма",
            screen = AlgorithmChannelScreen
        )
        NavigationButton(
            modifier = Modifier
                .padding(top = 16.dp)
                .padding(horizontal = 16.dp)
                .fillMaxWidth()
                .height(48.dp),
            header = "Помещения",
            screen = PremisesScreen
        )
        NavigationButton(
            modifier = Modifier
                .padding(top = 16.dp)
                .padding(horizontal = 16.dp)
                .fillMaxWidth()
                .height(48.dp),
            header = "Типовые механизмы",
            screen = TypicalMechanismsScreen
        )
        NavigationButton(
            modifier = Modifier
                .padding(top = 16.dp)
                .padding(horizontal = 16.dp)
                .fillMaxWidth()
                .height(48.dp),
            header = "Перечни",
            screen = ListsScreen
        )
        Spacer(modifier = Modifier.weight(1f))
        Button(
            modifier = Modifier
                .padding(vertical = 16.dp)
                .padding(horizontal = 16.dp)
                .fillMaxWidth()
                .height(48.dp),
            contentPadding = PaddingValues(horizontal = 16.dp),
            onClick = {
                preferencesSettings.remove(baseUrlKey)
                preferencesSettings.remove(loginKey)
                preferencesSettings.remove(passwordKey)
                currentScreen.value = LoginScreen
            },
        ) {
            Text(
                text = "Logout",
                color = MaterialTheme.colors.onPrimary
            )
        }
    }
}
