package sceens

import Screen
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import createTablesIfNotExists
import elements.BackToolbar
import elements.UserCheckbox
import elements.UserInputText
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import model.Premises
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.transactions.transaction
import sideEffect

object PremisesScreen : Screen {
    @Composable
    override fun draw() {
        View()
    }
}


@OptIn(ExperimentalFoundationApi::class)
@Composable
private fun View() {
    var items by remember {
        mutableStateOf<List<ResultRow>>(emptyList())
    }
    var changes by remember {
        mutableStateOf(0)
    }
    var nameEdit by remember { mutableStateOf("") }
    var roomNumberEdit by remember { mutableStateOf("") }
    var canBeUsedForDeviceEdit by remember { mutableStateOf(true) }
    val scope = rememberCoroutineScope()

    LaunchedEffect(changes) {
        withContext(Dispatchers.IO) {
            transaction {
                createTablesIfNotExists(
                    Premises
                )
                val table = Premises
                    .selectAll()
                    .orderBy(Premises.id, order = SortOrder.DESC)
                    .toList()
                items = table
            }
        }
    }
    Column(
        modifier = Modifier.fillMaxWidth(), horizontalAlignment = Alignment.CenterHorizontally
    ) {
        BackToolbar("Помещения")
        LazyColumn(
            modifier = Modifier.weight(1f).fillMaxWidth()
        ) {
            stickyHeader {
                Row(
                    modifier = Modifier
                        .background(MaterialTheme.colors.background)
                        .padding(
                            horizontal = 16.dp,
                            vertical = 4.dp
                        ),
                    horizontalArrangement = Arrangement.spacedBy(8.dp),
                    verticalAlignment = Alignment.CenterVertically,
                ) {
                    Text(
                        modifier = Modifier.weight(1f),
                        color = MaterialTheme.colors.onBackground,
                        text = "Id",
                    )
                    Text(
                        modifier = Modifier.weight(1f),
                        color = MaterialTheme.colors.onBackground,
                        text = "Name",
                    )
                    Text(
                        modifier = Modifier.weight(1f),
                        color = MaterialTheme.colors.onBackground,
                        text = "Room number",
                    )
                    Text(
                        modifier = Modifier.weight(1f),
                        color = MaterialTheme.colors.onBackground,
                        text = "For devices",
                    )
                    Text(
                        modifier = Modifier.minimumInteractiveComponentSize(),
                        color = MaterialTheme.colors.onBackground,
                        text = "Delete",
                    )
                }
            }
            itemsIndexed(items,
                key = { _, item -> item[Premises.id] }
            ) { index, result ->
                Row(
                    modifier = Modifier
                        .animateItemPlacement()
                        .background(
                            if (index % 2 == 0) {
                                MaterialTheme.colors.surface.copy(alpha = 0.3f)
                            } else {
                                MaterialTheme.colors.background
                            }
                        )
                        .padding(
                            horizontal = 16.dp,
                            vertical = 8.dp
                        ),
                    horizontalArrangement = Arrangement.spacedBy(8.dp),
                    verticalAlignment = Alignment.CenterVertically,
                ) {
                    Text(
                        modifier = Modifier.weight(1f),
                        color = MaterialTheme.colors.onBackground,
                        text = result[Premises.id].toString(),
                    )
                    Text(
                        modifier = Modifier.weight(1f),
                        color = MaterialTheme.colors.onBackground,
                        text = result[Premises.name],
                    )
                    Text(
                        modifier = Modifier.weight(1f),
                        color = MaterialTheme.colors.onBackground,
                        text = result[Premises.number].toString(),
                    )
                    Box(
                        modifier = Modifier.weight(1f),
                        contentAlignment = Alignment.CenterStart
                    ) {
                        Checkbox(
                            modifier = Modifier.fillMaxHeight(),
                            checked = result[Premises.canUsedForDevice],
                            onCheckedChange = {},
                            enabled = false
                        )
                    }
                    IconButton(onClick = {
                        scope.launch(Dispatchers.IO) {
                            runCatching {
                                transaction {
                                    Premises.deleteWhere {
                                        id eq result[id]
                                    }
                                }
                            }.onFailure {
                                sideEffect.emit(it.localizedMessage)
                            }.onSuccess {
                                changes++
                            }
                        }
                    }) {
                        Icon(
                            painter = painterResource("exit.svg"),
                            contentDescription = "Delete",
                            tint = MaterialTheme.colors.error,
                        )
                    }
                }
            }
        }
        Row(
            modifier = Modifier.padding(16.dp).fillMaxWidth(), horizontalArrangement = Arrangement.spacedBy(12.dp)
        ) {
            UserInputText(
                modifier = Modifier.weight(1f),
                header = "Id",
                textFieldValue = "Auto",
                onTextChanged = {}
            )
            UserInputText(
                modifier = Modifier.weight(1f),
                header = "Name",
                textFieldValue = nameEdit,
                onTextChanged = { nameEdit = it }
            )
            UserInputText(
                modifier = Modifier.weight(1f),
                header = "Room number",
                textFieldValue = roomNumberEdit,
                onTextChanged = { roomNumberEdit = it.filter { char -> char.isDigit() } }
            )
            UserCheckbox(
                modifier = Modifier.weight(1f),
                header = "Channels count",
                checked = canBeUsedForDeviceEdit,
                onChanged = { canBeUsedForDeviceEdit = !canBeUsedForDeviceEdit }
            )

        }
        Button(
            modifier = Modifier
                .padding(bottom = 16.dp)
                .padding(horizontal = 16.dp)
                .fillMaxWidth()
                .height(48.dp),
            enabled = nameEdit.isNotEmpty(),
            contentPadding = PaddingValues(horizontal = 16.dp),
            onClick = {
                scope.launch(Dispatchers.IO) {
                    runCatching {
                        transaction {
                            Premises.insert { statement ->
                                statement[name] = nameEdit
                                statement[number] = roomNumberEdit
                                statement[canUsedForDevice] = canBeUsedForDeviceEdit
                            }
                        }
                    }.onFailure {
                        sideEffect.emit(it.localizedMessage)
                    }.onSuccess {
                        nameEdit = ""
                        roomNumberEdit = ""
                        canBeUsedForDeviceEdit = true
                        changes++
                    }
                }
            },
        ) {
            Text(
                text = "Add", color = MaterialTheme.colors.onPrimary
            )
        }
    }
}
