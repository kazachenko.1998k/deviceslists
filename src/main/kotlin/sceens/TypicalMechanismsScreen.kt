package sceens

import Screen
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import createTablesIfNotExists
import elements.BackToolbar
import elements.DropdownView
import elements.UserInputText
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import model.TypicalMechanisms
import model.TypicalMechanismsSignalName
import model.TypicalMechanismsSignalType
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.transactions.transaction
import sideEffect

object TypicalMechanismsScreen : Screen {
    @Composable
    override fun draw() {
        View()
    }
}


@OptIn(ExperimentalFoundationApi::class)
@Composable
private fun View() {
    var items by remember {
        mutableStateOf<List<ResultRow>>(emptyList())
    }
    var changes by remember {
        mutableStateOf(0)
    }
    var tableCreated by remember { mutableStateOf(false) }
    var nameEdit by remember { mutableStateOf("") }
    var inputCountEdit by remember { mutableStateOf("") }
    var outputCountEdit by remember { mutableStateOf("") }
    var signalNameEdit by remember { mutableStateOf("") }
    var signalTypeEdit by remember { mutableStateOf("") }
    val scope = rememberCoroutineScope()

    LaunchedEffect(changes) {
        withContext(Dispatchers.IO) {
            transaction {
                createTablesIfNotExists(
                    TypicalMechanisms,
                    TypicalMechanismsSignalName,
                    TypicalMechanismsSignalType
                )
                val table = TypicalMechanisms
                    .innerJoin(TypicalMechanismsSignalName)
                    .innerJoin(TypicalMechanismsSignalType)
                    .selectAll()
                    .orderBy(TypicalMechanisms.name)
                    .toList()
                items = table
            }
            tableCreated = true
        }
    }
    Column(
        modifier = Modifier.fillMaxWidth(), horizontalAlignment = Alignment.CenterHorizontally
    ) {
        BackToolbar("Типовые механизмы")
        LazyColumn(
            modifier = Modifier.weight(1f).fillMaxWidth()
        ) {
            stickyHeader {
                Row(
                    modifier = Modifier
                        .background(MaterialTheme.colors.background)
                        .padding(
                            horizontal = 16.dp,
                            vertical = 4.dp
                        ),
                    horizontalArrangement = Arrangement.spacedBy(8.dp),
                    verticalAlignment = Alignment.CenterVertically,
                ) {
                    Text(
                        modifier = Modifier.weight(1f),
                        color = MaterialTheme.colors.onBackground,
                        text = "Name",
                    )
                    Text(
                        modifier = Modifier.weight(1f),
                        color = MaterialTheme.colors.onBackground,
                        text = "Input count",
                    )
                    Text(
                        modifier = Modifier.weight(1f),
                        color = MaterialTheme.colors.onBackground,
                        text = "Output count",
                    )
                    Text(
                        modifier = Modifier.weight(1f),
                        color = MaterialTheme.colors.onBackground,
                        text = "Signal name",
                    )
                    Text(
                        modifier = Modifier.weight(1f),
                        color = MaterialTheme.colors.onBackground,
                        text = "Signal type",
                    )
                    Text(
                        modifier = Modifier.minimumInteractiveComponentSize(),
                        color = MaterialTheme.colors.onBackground,
                        text = "Delete",
                    )
                }
            }
            itemsIndexed(items,
                key = { _, item -> item[TypicalMechanisms.name] }
            ) { index, result ->
                Row(
                    modifier = Modifier
                        .animateItemPlacement()
                        .background(
                            if (index % 2 == 0) {
                                MaterialTheme.colors.surface.copy(alpha = 0.3f)
                            } else {
                                MaterialTheme.colors.background
                            }
                        )
                        .padding(
                            horizontal = 16.dp,
                            vertical = 8.dp
                        ),
                    horizontalArrangement = Arrangement.spacedBy(8.dp),
                    verticalAlignment = Alignment.CenterVertically,
                ) {
                    Text(
                        modifier = Modifier.weight(1f),
                        color = MaterialTheme.colors.onBackground,
                        text = result[TypicalMechanisms.name],
                    )
                    Text(
                        modifier = Modifier.weight(1f),
                        color = MaterialTheme.colors.onBackground,
                        text = result[TypicalMechanisms.inputCount].toString(),
                    )
                    Text(
                        modifier = Modifier.weight(1f),
                        color = MaterialTheme.colors.onBackground,
                        text = result[TypicalMechanisms.outputCount].toString(),
                    )
                    Text(
                        modifier = Modifier.weight(1f),
                        color = MaterialTheme.colors.onBackground,
                        text = result[TypicalMechanismsSignalName.name],
                    )
                    Text(
                        modifier = Modifier.weight(1f),
                        color = MaterialTheme.colors.onBackground,
                        text = result[TypicalMechanismsSignalType.name],
                    )
                    IconButton(onClick = {
                        scope.launch(Dispatchers.IO) {
                            runCatching {
                                transaction {
                                    TypicalMechanisms.deleteWhere {
                                        name eq result[name]
                                    }
                                }
                            }.onFailure {
                                sideEffect.emit(it.localizedMessage)
                            }.onSuccess {
                                changes++
                            }
                        }
                    }) {
                        Icon(
                            painter = painterResource("exit.svg"),
                            contentDescription = "Delete",
                            tint = MaterialTheme.colors.error,
                        )
                    }
                }
            }
        }
        Row(
            modifier = Modifier.padding(16.dp).fillMaxWidth(), horizontalArrangement = Arrangement.spacedBy(12.dp)
        ) {
            UserInputText(
                modifier = Modifier.weight(1f),
                header = "Name",
                textFieldValue = nameEdit,
                onTextChanged = { nameEdit = it }
            )
            UserInputText(
                modifier = Modifier.weight(1f),
                header = "Input count",
                textFieldValue = inputCountEdit,
                onTextChanged = { inputCountEdit = it.filter { char -> char.isDigit() } }
            )
            UserInputText(
                modifier = Modifier.weight(1f),
                header = "Output count",
                textFieldValue = outputCountEdit,
                onTextChanged = { outputCountEdit = it.filter { char -> char.isDigit() } }
            )
            DropdownView(
                modifier = Modifier.weight(1f),
                header = "Signal name",
                table = TypicalMechanismsSignalName,
                selected = signalNameEdit,
                onSelectNew = { signalNameEdit = it },
                tableCreated = tableCreated,
                onDelete = {
                    changes++
                },
            )
            DropdownView(
                modifier = Modifier.weight(1f),
                header = "Signal type",
                table = TypicalMechanismsSignalType,
                selected = signalTypeEdit,
                onSelectNew = { signalTypeEdit = it },
                tableCreated = tableCreated,
                onDelete = {
                    changes++
                },
            )
        }
        Button(
            modifier = Modifier
                .padding(bottom = 16.dp)
                .padding(horizontal = 16.dp)
                .fillMaxWidth()
                .height(48.dp),
            contentPadding = PaddingValues(horizontal = 16.dp),
            enabled =
            nameEdit.isNotEmpty() &&
                    inputCountEdit.isNotEmpty() &&
                    outputCountEdit.isNotEmpty() &&
                    signalNameEdit.isNotEmpty() &&
                    signalTypeEdit.isNotEmpty(),
            onClick = {
                scope.launch(Dispatchers.IO) {
                    runCatching {
                        transaction {
                            TypicalMechanisms.insert { statement ->
                                statement[name] = nameEdit
                                statement[inputCount] = inputCountEdit.toInt()
                                statement[outputCount] = outputCountEdit.toInt()
                                statement[signalName] =
                                    TypicalMechanismsSignalName
                                        .select { TypicalMechanismsSignalName.name eq signalNameEdit }
                                        .single()[TypicalMechanismsSignalName.id]
                                statement[signalType] =
                                    TypicalMechanismsSignalType
                                        .select { TypicalMechanismsSignalType.name eq signalTypeEdit }
                                        .single()[TypicalMechanismsSignalType.id]
                            }
                        }
                    }.onFailure {
                        sideEffect.emit(it.localizedMessage)
                    }.onSuccess {
                        nameEdit = ""
                        inputCountEdit = ""
                        outputCountEdit = ""
                        signalNameEdit = ""
                        signalTypeEdit = ""
                        changes++
                    }
                }
            },
        ) {
            Text(
                text = "Add", color = MaterialTheme.colors.onPrimary
            )
        }
    }
}
