package sceens

import Screen
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import elements.BackToolbar
import elements.DropdownView
import elements.UserCheckbox
import elements.UserInputText
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import model.*
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.transactions.transaction
import sideEffect

object ListsAddScreen : Screen {
    @Composable
    override fun draw() {
        View()
    }

    @Composable
    private fun View() {
        Column(
            modifier = Modifier.fillMaxWidth(),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            BackToolbar("Добавление нового оборудования", ListsScreen)
            var scope = rememberCoroutineScope()
            var identifierEdit by remember { mutableStateOf("") }
            var placeCKNumberEdit by remember { mutableStateOf("") }
            var placePNumberEdit by remember { mutableStateOf("") }
            var placeCKNameEdit by remember { mutableStateOf("") }
            var placePNameEdit by remember { mutableStateOf("") }
            var securityLevelEdit by remember { mutableStateOf("") }
            var channelAlgEdit by remember { mutableStateOf("") }
            var typeEdit by remember { mutableStateOf("") }
            var boardEdit by remember { mutableStateOf("") }
            var secondConverterEdit by remember { mutableStateOf("") }
            var deviceTypeCKEdit by remember { mutableStateOf("") }
            var deviceTypePEdit by remember { mutableStateOf("") }
            var mechanismTypeEdit by remember { mutableStateOf("") }
            var signalNameEdit by remember { mutableStateOf("") }
            var typeSignalEdit by remember { mutableStateOf("") }
            var additionalCableEdit by remember { mutableStateOf(false) }
            var energyEdit by remember { mutableStateOf(false) }
            var participationEdit by remember { mutableStateOf(false) }
            LaunchedEffect(placeCKNumberEdit) {
                withContext(Dispatchers.IO) {
                    transaction {
                        val placeCK = Premises.select { Premises.number eq placeCKNumberEdit }.singleOrNull()
                        placeCKNameEdit = placeCK?.let { it[Premises.name] } ?: notFoundText
                    }
                }
            }
            LaunchedEffect(placePNumberEdit) {
                withContext(Dispatchers.IO) {
                    transaction {
                        val placeCK = Premises.select { Premises.number eq placePNumberEdit }.singleOrNull()
                        placePNameEdit = placeCK?.let { it[Premises.name] } ?: notFoundText
                    }
                }
            }
            LaunchedEffect(identifierEdit) {
                withContext(Dispatchers.IO) {
                    transaction {
                        val algorithm =
                            AlgorithmChannel.select { AlgorithmChannel.name eq identifierEdit }.singleOrNull()
                        channelAlgEdit = algorithm?.let { it[AlgorithmChannel.name] } ?: notFoundText
                        val participation = Participation.select { Participation.name eq identifierEdit }.singleOrNull()
                        participationEdit = participation != null
                    }
                }
            }
            LaunchedEffect(mechanismTypeEdit) {
                withContext(Dispatchers.IO) {
                    transaction {
                        val mechanism = TypicalMechanisms
                            .innerJoin(TypicalMechanismsSignalName)
                            .innerJoin(TypicalMechanismsSignalType)
                            .select { TypicalMechanisms.name eq mechanismTypeEdit }
                            .singleOrNull()
                        signalNameEdit = mechanism?.get(TypicalMechanismsSignalName.name) ?: notFoundText
                        typeSignalEdit = mechanism?.get(TypicalMechanismsSignalType.name) ?: notFoundText
                    }
                }
            }
            Column(
                modifier = Modifier.weight(1f).verticalScroll(rememberScrollState()),
                verticalArrangement = Arrangement.spacedBy(16.dp),
            ) {
                UserInputText(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(horizontal = 16.dp)
                        .padding(top = 16.dp),
                    header = "Идентификатор",
                    textFieldValue = identifierEdit,
                    onTextChanged = { identifierEdit = it }
                )
                Row(
                    modifier = Modifier.padding(horizontal = 16.dp),
                    horizontalArrangement = Arrangement.spacedBy(12.dp)
                ) {
                    UserInputText(
                        modifier = Modifier.weight(1f),
                        header = "Расположение СК (БК)",
                        textFieldValue = placeCKNumberEdit,
                        onTextChanged = { placeCKNumberEdit = it }
                    )
                    UserInputText(
                        modifier = Modifier.weight(1f),
                        header = "Имя",
                        textFieldValue = placeCKNameEdit,
                        onTextChanged = { }
                    )
                }
                Row(
                    modifier = Modifier.padding(horizontal = 16.dp),
                    horizontalArrangement = Arrangement.spacedBy(12.dp)

                ) {
                    UserInputText(
                        modifier = Modifier.weight(1f),
                        header = "Расположение Р (М, ПМ)",
                        textFieldValue = placePNumberEdit,
                        onTextChanged = { placePNumberEdit = it }
                    )
                    UserInputText(
                        modifier = Modifier.weight(1f),
                        header = "Имя",
                        textFieldValue = placePNameEdit,
                        onTextChanged = { placePNameEdit = it }
                    )
                }
                DropdownView(
                    modifier = Modifier.fillMaxWidth().padding(horizontal = 16.dp),
                    header = "Класс безопасности",
                    table = SecurityLevel,
                    tableCreated = true,
                    selected = securityLevelEdit,
                    onSelectNew = { securityLevelEdit = it },
                    onDelete = {},
                )
                DropdownView(
                    modifier = Modifier.fillMaxWidth().padding(horizontal = 16.dp),
                    header = "Канал алгоритма",
                    table = AlgorithmChannel,
                    tableCreated = true,
                    selected = channelAlgEdit,
                    onSelectNew = { channelAlgEdit = it },
                    onDelete = {},
                )
                DropdownView(
                    modifier = Modifier.fillMaxWidth().padding(horizontal = 16.dp),
                    header = "Тип",
                    table = ListsType,
                    tableCreated = true,
                    selected = typeEdit,
                    onSelectNew = { typeEdit = it },
                    onDelete = {},
                )
                UserInputText(
                    modifier = Modifier.fillMaxWidth().padding(horizontal = 16.dp),
                    header = "Борт",
                    textFieldValue = boardEdit,
                    onTextChanged = { boardEdit = it.filter { it.isDigit() } }
                )
                UserCheckbox(
                    modifier = Modifier.padding(horizontal = 16.dp),
                    header = "Питание",
                    checkboxAlignment = Alignment.Start,
                    checked = energyEdit,
                    onChanged = { energyEdit = it }
                )
                UserCheckbox(
                    modifier = Modifier.padding(horizontal = 16.dp),
                    header = "Участие в АЗ",
                    checked = participationEdit,
                    checkboxAlignment = Alignment.Start,
                    enabled = false,
                    onChanged = { participationEdit = it }
                )
                UserCheckbox(
                    modifier = Modifier.padding(horizontal = 16.dp),
                    header = "Доп кабель",
                    checked = additionalCableEdit,
                    checkboxAlignment = Alignment.Start,
                    onChanged = { additionalCableEdit = it }
                )
                DropdownView(
                    modifier = Modifier.fillMaxWidth().padding(horizontal = 16.dp),
                    header = "Вторичный преобразователь",
                    table = SecondaryConverter,
                    tableCreated = true,
                    selected = secondConverterEdit,
                    onSelectNew = { secondConverterEdit = it },
                    onDelete = {},
                )
                DropdownView(
                    modifier = Modifier.fillMaxWidth().padding(horizontal = 16.dp),
                    header = "Тип прибора СК (БК)",
                    table = Devices,
                    tableCreated = true,
                    selected = deviceTypeCKEdit,
                    onSelectNew = { deviceTypeCKEdit = it },
                    onDelete = {},
                )
                DropdownView(
                    modifier = Modifier.fillMaxWidth().padding(horizontal = 16.dp),
                    header = "Тип прибора Р (М, ПМ)",
                    table = Devices,
                    tableCreated = true,
                    selected = deviceTypePEdit,
                    onSelectNew = { deviceTypePEdit = it },
                    onDelete = {},
                )
                Row(
                    modifier = Modifier.fillMaxWidth()
                        .padding(horizontal = 16.dp)
                        .padding(bottom = 16.dp),
                    horizontalArrangement = Arrangement.spacedBy(12.dp)
                ) {
                    DropdownView(
                        modifier = Modifier.weight(1f),
                        header = "Тип механизма",
                        table = TypicalMechanisms,
                        tableCreated = true,
                        selected = mechanismTypeEdit,
                        onSelectNew = { mechanismTypeEdit = it },
                    )
                    UserInputText(
                        modifier = Modifier.weight(1f),
                        header = "Имя сигнала",
                        textFieldValue = signalNameEdit.ifEmpty { notFoundText },
                        onTextChanged = { }
                    )
                    UserInputText(
                        modifier = Modifier.weight(1f),
                        header = "Тип сигнала (AI AO DO DI)",
                        textFieldValue = typeSignalEdit.ifEmpty { notFoundText },
                        onTextChanged = { }
                    )
                }

            }
            Button(
                modifier = Modifier
                    .padding(bottom = 16.dp)
                    .padding(horizontal = 16.dp)
                    .fillMaxWidth()
                    .height(48.dp),
                contentPadding = PaddingValues(horizontal = 16.dp),
                enabled =
                identifierEdit.isNotEmpty() &&
                        placeCKNumberEdit.isNotEmpty() &&
                        placePNumberEdit.isNotEmpty() &&
                        placeCKNameEdit.isNotEmpty() &&
                        placeCKNameEdit != notFoundText &&
                        placePNameEdit != notFoundText &&
                        placePNameEdit.isNotEmpty() &&
                        securityLevelEdit.isNotEmpty() &&
                        channelAlgEdit.isNotEmpty() &&
                        typeEdit.isNotEmpty() &&
                        boardEdit.isNotEmpty() &&
                        secondConverterEdit.isNotEmpty() &&
                        deviceTypeCKEdit.isNotEmpty() &&
                        deviceTypePEdit.isNotEmpty() &&
                        mechanismTypeEdit.isNotEmpty() &&
                        signalNameEdit.isNotEmpty() &&
                        signalNameEdit != notFoundText &&
                        typeSignalEdit.isNotEmpty() &&
                        typeSignalEdit != notFoundText,
                onClick = {
                    scope.launch(Dispatchers.IO) {
                        runCatching {
                            transaction {
                                Lists.insert {
                                    it[name] = identifierEdit
                                    it[positionCK] =
                                        Premises.select { Premises.number eq placeCKNumberEdit }
                                            .single()[Premises.id]
                                    it[positionP] =
                                        Premises.select { Premises.number eq placePNumberEdit }
                                            .single()[Premises.id]
                                    it[securityLevel] =
                                        SecurityLevel.select { SecurityLevel.name eq securityLevelEdit }
                                            .single()[SecurityLevel.id]
                                    it[algorithmChannel] =
                                        AlgorithmChannel.select { AlgorithmChannel.name eq channelAlgEdit }
                                            .single()[AlgorithmChannel.id]
                                    it[type] =
                                        ListsType.select { ListsType.name eq typeEdit }
                                            .single()[ListsType.id]
                                    it[board] = boardEdit.toInt()
                                    it[energy] = energyEdit
                                    it[participation] =
                                        Participation.select { Participation.name eq identifierEdit }
                                            .singleOrNull()?.get(Participation.id)
                                    it[additionalCable] = additionalCableEdit
                                    it[secondaryConverter] =
                                        SecondaryConverter.select { SecondaryConverter.name eq secondConverterEdit }
                                            .single()[SecondaryConverter.id]
                                    it[deviceCK] = Devices.select { Devices.name eq deviceTypeCKEdit }
                                        .single()[Devices.id]
                                    it[deviceP] = Devices.select { Devices.name eq deviceTypePEdit }
                                        .single()[Devices.id]
                                    it[mechanismType] = mechanismTypeEdit
                                }
                            }
                        }.onFailure {
                            sideEffect.emit(it.localizedMessage)
                        }.onSuccess {
                            sideEffect.emit("Успешно добавлено")
                            identifierEdit = ""
                            placeCKNumberEdit = ""
                            placePNumberEdit = ""
                            placeCKNameEdit = ""
                            placePNameEdit = ""
                            securityLevelEdit = ""
                            channelAlgEdit = ""
                            typeEdit = ""
                            boardEdit = ""
                            additionalCableEdit = false
                            energyEdit = false
                            participationEdit = false
                            secondConverterEdit = ""
                            deviceTypeCKEdit = ""
                            deviceTypePEdit = ""
                            mechanismTypeEdit = ""
                            signalNameEdit = ""
                            typeSignalEdit = ""
                        }
                    }
                },
            ) {
                Text(
                    text = "Добавить", color = MaterialTheme.colors.onPrimary
                )
            }
        }
    }
}


val notFoundText = "Не найдено"