package sceens

import Screen
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.onFocusChanged
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import createTablesIfNotExists
import elements.BackToolbar
import elements.DropdownView
import elements.MultiSelectDropdownView
import elements.UserInputText
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import model.*
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.transactions.transaction
import sideEffect

object DevicesScreen : Screen {
    @Composable
    override fun draw() {
        View()
    }
}


@OptIn(ExperimentalFoundationApi::class)
@Composable
private fun View() {
    var items by remember {
        mutableStateOf<List<DeviceModel>>(emptyList())
    }
    var changes by remember {
        mutableStateOf(0)
    }
    var tableCreated by remember { mutableStateOf(false) }
    var typeEdit by remember { mutableStateOf("") }
    var aiFieldEdit by remember { mutableStateOf("") }
    var aoFieldEdit by remember { mutableStateOf("") }
    var diFieldEdit by remember { mutableStateOf("") }
    var doFieldEdit by remember { mutableStateOf("") }
    var relayFieldEdit by remember { mutableStateOf("") }
    val terminalFieldEdit = remember { mutableStateListOf<String>() }
    val scope = rememberCoroutineScope()

    LaunchedEffect(changes) {
        withContext(Dispatchers.IO) {
            transaction {
                createTablesIfNotExists(
                    Devices,
                    AIField,
                    AOField,
                    DIField,
                    DOField,
                    RelayField,
                    TerminalsToDevicesField,
                )
                val table = Devices
                    .leftJoin(AIField)
                    .leftJoin(AOField)
                    .leftJoin(DIField)
                    .leftJoin(DOField)
                    .leftJoin(RelayField)
                    .leftJoin(TerminalsToDevicesField)
                    .leftJoin(TerminalField)
                    .slice(
                        Devices.id,
                        Devices.name,
                        AIField.name,
                        AOField.name,
                        DIField.name,
                        DOField.name,
                        RelayField.name,
                        TerminalField.name
                    )
                    .selectAll()
                    .orderBy(Devices.name, order = SortOrder.DESC)
                    .groupBy { it[Devices.name] }
                    .map { (key, results) ->
                        DeviceModel(
                            name = key,
                            aiField = results.first()[AIField.name],
                            aoField = results.first()[AOField.name],
                            diField = results.first()[DIField.name],
                            doField = results.first()[DOField.name],
                            relayField = results.first()[RelayField.name],
                            terminals = @Suppress("UselessCallOnCollection")
                            results.mapNotNull { it[TerminalField.name] },
                        )
                    }
                items = table
            }
            tableCreated = true
        }
    }
    Column(
        modifier = Modifier.fillMaxWidth(), horizontalAlignment = Alignment.CenterHorizontally
    ) {
        BackToolbar("Приборы")
        LazyColumn(
            modifier = Modifier.weight(1f).fillMaxWidth()
        ) {
            stickyHeader {
                Row(
                    modifier = Modifier
                        .background(MaterialTheme.colors.background)
                        .padding(
                            horizontal = 16.dp,
                            vertical = 4.dp
                        ),
                    horizontalArrangement = Arrangement.spacedBy(8.dp),
                    verticalAlignment = Alignment.CenterVertically,
                ) {
                    Text(
                        modifier = Modifier.weight(0.5f),
                        color = MaterialTheme.colors.onBackground,
                        text = "Id",
                    )
                    Text(
                        modifier = Modifier.weight(1f),
                        color = MaterialTheme.colors.onBackground,
                        text = "Type",
                    )
                    Text(
                        modifier = Modifier.weight(1f),
                        color = MaterialTheme.colors.onBackground,
                        text = "AI",
                    )
                    Text(
                        modifier = Modifier.weight(1f),
                        color = MaterialTheme.colors.onBackground,
                        text = "AO",
                    )
                    Text(
                        modifier = Modifier.weight(1f),
                        color = MaterialTheme.colors.onBackground,
                        text = "DI",
                    )
                    Text(
                        modifier = Modifier.weight(1f),
                        color = MaterialTheme.colors.onBackground,
                        text = "DO",
                    )
                    Text(
                        modifier = Modifier.weight(1f),
                        color = MaterialTheme.colors.onBackground,
                        text = "Relay",
                    )
                    Text(
                        modifier = Modifier.weight(1f),
                        color = MaterialTheme.colors.onBackground,
                        text = "Terminals",
                    )
                    Text(
                        modifier = Modifier.minimumInteractiveComponentSize(),
                        color = MaterialTheme.colors.onBackground,
                        text = "Delete",
                    )
                }
            }
            itemsIndexed(items,
                key = { _, item -> item.name }
            ) { index, result ->
                Row(
                    modifier = Modifier
                        .animateItemPlacement()
                        .background(
                            if (index % 2 == 0) {
                                MaterialTheme.colors.surface.copy(alpha = 0.3f)
                            } else {
                                MaterialTheme.colors.background
                            }
                        )
                        .padding(
                            horizontal = 16.dp,
                            vertical = 8.dp
                        ),
                    horizontalArrangement = Arrangement.spacedBy(8.dp),
                    verticalAlignment = Alignment.CenterVertically,
                ) {
                    Text(
                        modifier = Modifier.weight(1f),
                        color = MaterialTheme.colors.onBackground,
                        text = result.name,
                    )
                    Text(
                        modifier = Modifier.weight(1f),
                        color = MaterialTheme.colors.onBackground,
                        text = result.aiField.orEmpty(),
                    )
                    Text(
                        modifier = Modifier.weight(1f),
                        color = MaterialTheme.colors.onBackground,
                        text = result.aoField.orEmpty(),
                    )
                    Text(
                        modifier = Modifier.weight(1f),
                        color = MaterialTheme.colors.onBackground,
                        text = result.diField.orEmpty(),
                    )
                    Text(
                        modifier = Modifier.weight(1f),
                        color = MaterialTheme.colors.onBackground,
                        text = result.doField.orEmpty(),
                    )
                    Text(
                        modifier = Modifier.weight(1f),
                        color = MaterialTheme.colors.onBackground,
                        text = result.relayField.orEmpty(),
                    )
                    Text(
                        modifier = Modifier.weight(1f).onFocusChanged { },
                        color = MaterialTheme.colors.onBackground,
                        text = result.terminals.joinToString(),
                    )
                    IconButton(onClick = {
                        scope.launch(Dispatchers.IO) {
                            runCatching {
                                transaction {
                                    Devices.deleteWhere {
                                        name eq result.name
                                    }
                                }
                            }.onFailure {
                                sideEffect.emit(it.localizedMessage)
                            }.onSuccess {
                                changes++
                            }
                        }
                    }) {
                        Icon(
                            painter = painterResource("exit.svg"),
                            contentDescription = "Delete",
                            tint = MaterialTheme.colors.error,
                        )
                    }
                }
            }
        }
        Row(
            modifier = Modifier.padding(16.dp).fillMaxWidth(), horizontalArrangement = Arrangement.spacedBy(12.dp)
        ) {
            UserInputText(
                modifier = Modifier.weight(1f),
                header = "Type",
                textFieldValue = typeEdit,
                onTextChanged = { typeEdit = it }
            )
            DropdownView(modifier = Modifier.weight(1f),
                header = "AI",
                table = AIField,
                selected = aiFieldEdit,
                onDelete = { changes++ },
                tableCreated = tableCreated,
                onSelectNew = { aiFieldEdit = it })
            DropdownView(modifier = Modifier.weight(1f),
                header = "AO",
                table = AOField,
                selected = aoFieldEdit,
                onDelete = { changes++ },
                tableCreated = tableCreated,
                onSelectNew = { aoFieldEdit = it })
            DropdownView(modifier = Modifier.weight(1f),
                header = "DI",
                table = DIField,
                selected = diFieldEdit,
                onDelete = { changes++ },
                tableCreated = tableCreated,
                onSelectNew = { diFieldEdit = it })
            DropdownView(modifier = Modifier.weight(1f),
                header = "DO",
                table = DOField,
                selected = doFieldEdit,
                onDelete = { changes++ },
                tableCreated = tableCreated,
                onSelectNew = { doFieldEdit = it })
            DropdownView(modifier = Modifier.weight(1f),
                header = "Relay",
                table = RelayField,
                selected = relayFieldEdit,
                onDelete = { changes++ },
                tableCreated = tableCreated,
                onSelectNew = { relayFieldEdit = it })
            MultiSelectDropdownView(modifier = Modifier.weight(1f),
                header = "Terminals",
                table = TerminalField,
                selected = terminalFieldEdit,
                onDelete = { changes++ },
                tableCreated = tableCreated,
                onSelectNew = {
                    terminalFieldEdit.add(it)
                },
                onDeselectNew = {
                    terminalFieldEdit.remove(it)
                }
            )
        }
        Button(
            modifier = Modifier.padding(bottom = 16.dp).padding(horizontal = 16.dp).fillMaxWidth().height(48.dp),
            contentPadding = PaddingValues(horizontal = 16.dp),
            enabled = typeEdit.isNotEmpty() && (sequenceOf(
                aiFieldEdit,
                aoFieldEdit,
                diFieldEdit,
                doFieldEdit,
                relayFieldEdit
            ).any { it.isNotEmpty() } || terminalFieldEdit.isNotEmpty()),
            onClick = {
                scope.launch(Dispatchers.IO) {
                    runCatching {
                        transaction {
                            val newDeviceId = Devices.insertAndGetId { statement ->
                                statement[name] = typeEdit
                                statement[aiField] =
                                    AIField.select { AIField.name eq aiFieldEdit }.singleOrNull()?.get(AIField.id)
                                statement[aoField] =
                                    AOField.select { AOField.name eq aoFieldEdit }.singleOrNull()?.get(AOField.id)
                                statement[diField] =
                                    DIField.select { DIField.name eq diFieldEdit }.singleOrNull()?.get(DIField.id)
                                statement[doField] =
                                    DOField.select { DOField.name eq doFieldEdit }.singleOrNull()?.get(DOField.id)
                                statement[relayField] =
                                    RelayField.select { RelayField.name eq relayFieldEdit }.singleOrNull()
                                        ?.get(RelayField.id)
                            }.value
                            terminalFieldEdit.forEach { terminalName ->
                                val terminalId = TerminalField.select { TerminalField.name eq terminalName }
                                    .single()[TerminalField.id]
                                TerminalsToDevicesField.insert {
                                    it[terminal] = terminalId
                                    it[device] = newDeviceId
                                }
                            }
                        }
                    }.onFailure {
                        sideEffect.emit(it.localizedMessage)
                    }.onSuccess {
                        typeEdit = ""
                        aiFieldEdit = ""
                        aoFieldEdit = ""
                        diFieldEdit = ""
                        doFieldEdit = ""
                        relayFieldEdit = ""
                        terminalFieldEdit.clear()
                        changes++
                    }
                }
            },
        ) {
            Text(
                text = "Add", color = MaterialTheme.colors.onPrimary
            )
        }
    }
}
