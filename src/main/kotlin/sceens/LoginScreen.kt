package sceens

import Const.baseUrlKey
import Const.loginKey
import Const.passwordKey
import Screen
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import currentScreen
import elements.UserInputText
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.DatabaseConfig
import org.jetbrains.exposed.sql.StdOutSqlLogger
import preferencesSettings
import sideEffect

object LoginScreen : Screen {
    @Composable
    override fun draw() {
        LoginView()
    }

}

@Composable
fun LoginView() {
    var baseUrl by remember { mutableStateOf(preferencesSettings.getString(baseUrlKey, "")) }
    var login by remember { mutableStateOf(preferencesSettings.getString(loginKey, "")) }
    var password by remember { mutableStateOf(preferencesSettings.getString(passwordKey, "")) }
    val scope = rememberCoroutineScope()
    LaunchedEffect(Unit) {
        if (baseUrl.isNotEmpty() && login.isNotEmpty() && password.isNotEmpty()) {
            tryConnect(baseUrl, login, password)
        }
    }
    Column(
        modifier = Modifier.fillMaxWidth(),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(
            modifier = Modifier
                .padding(top = 24.dp)
                .padding(horizontal = 16.dp),
            text = "Введите данные для поключения к базе данных",
            color = MaterialTheme.colors.onBackground,
            style = MaterialTheme.typography.h4
        )
        UserInputText(
            modifier = Modifier
                .padding(16.dp)
                .fillMaxWidth(),
            header = "URL для подключения",
            textFieldValue = baseUrl,
            onTextChanged = { baseUrl = it }
        )
        UserInputText(
            modifier = Modifier
                .padding(16.dp)
                .fillMaxWidth(),
            header = "Логин/Роль",
            textFieldValue = login,
            onTextChanged = { login = it }
        )
        UserInputText(
            modifier = Modifier
                .padding(16.dp)
                .fillMaxWidth(),
            header = "Пароль",
            textFieldValue = password,
            onTextChanged = { password = it }
        )
        Button(
            onClick = {
                scope.launch {
                    tryConnect(baseUrl, login, password)
                }
            },
            contentPadding = PaddingValues(horizontal = 16.dp),
            enabled = baseUrl.isNotEmpty() && login.isNotEmpty() && password.isNotEmpty(),
        ) {
            Text(
                text = "Подключние",
                color = MaterialTheme.colors.onPrimary
            )
        }
    }
}

private suspend fun tryConnect(
    baseUrl: String = "jdbc:pgsql://localhost:5432/db",
    login: String = "postgres",
    password: String = "1234"
) {
    withContext(Dispatchers.IO) {
        runCatching {
            Database.connect(
                baseUrl,
                user = login,
                password = password,
                databaseConfig = DatabaseConfig.invoke {
                    sqlLogger = StdOutSqlLogger//TODO
                }
            )
        }.onSuccess {
            sideEffect.emit("Успешное подключение к БД")
            preferencesSettings.putString(baseUrlKey, baseUrl)
            preferencesSettings.putString(loginKey, login)
            preferencesSettings.putString(passwordKey, password)
            currentScreen.value = HomeScreen
        }.onFailure {
            sideEffect.emit(it.localizedMessage ?: "Ошибка в соединении")
        }
    }
}
