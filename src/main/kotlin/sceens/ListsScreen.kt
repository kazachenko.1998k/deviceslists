package sceens

import Screen
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.TooltipArea
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import createTablesIfNotExists
import currentScreen
import elements.BackToolbar
import elements.SearchText
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import model.*
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.transactions.transaction
import sideEffect

object ListsScreen : Screen {
    @Composable
    override fun draw() {
        View()
    }
}


@OptIn(ExperimentalFoundationApi::class)
@Composable
private fun View() {
    var items by remember {
        mutableStateOf<List<ResultRow>>(emptyList())
    }
    var changes by remember {
        mutableStateOf(0)
    }
    var tableCreated by remember { mutableStateOf(false) }
    val scope = rememberCoroutineScope()
    val positionP = Premises.alias("position_premises_p")
    val devicesP = Devices.alias("devices_p")
    var searchText by remember { mutableStateOf("") }
    LaunchedEffect(changes, searchText) {
        withContext(Dispatchers.IO) {
            transaction {
                createTablesIfNotExists(
                    Lists,
                    AlgorithmChannel,
                    SecurityLevel,
                    ListsType,
                    SecondaryConverter,
                    Devices,
                    AIField,
                    AOField,
                    DIField,
                    DOField,
                    RelayField,
                    TerminalsToDevicesField,
                    Participation,
                    Premises,
                    TypicalMechanisms,
                    TypicalMechanismsSignalName,
                    TypicalMechanismsSignalType
                )
                tableCreated = true
                val table = Lists
                    .join(Premises, joinType = JoinType.LEFT, onColumn = Premises.id, otherColumn = Lists.positionCK)
                    .join(
                        positionP,
                        joinType = JoinType.LEFT,
                        onColumn = positionP[Premises.id],
                        otherColumn = Lists.positionP
                    )
                    .join(Devices, joinType = JoinType.LEFT, onColumn = Devices.id, otherColumn = Lists.deviceCK)
                    .join(
                        devicesP,
                        joinType = JoinType.LEFT,
                        onColumn = devicesP[Devices.id],
                        otherColumn = Lists.deviceP
                    )
                    .innerJoin(SecurityLevel)
                    .innerJoin(AlgorithmChannel)
                    .innerJoin(ListsType)
                    .leftJoin(Participation)
                    .innerJoin(SecondaryConverter)
                    .innerJoin(TypicalMechanisms)
                    .innerJoin(TypicalMechanismsSignalName)
                    .innerJoin(TypicalMechanismsSignalType)
                    .select { Lists.name like "%$searchText%" }
                    .orderBy(Lists.name)
                    .toList()
                items = table
            }
        }
    }
    Column(
        modifier = Modifier.fillMaxWidth(), horizontalAlignment = Alignment.CenterHorizontally
    ) {
        BackToolbar("Перечни") {
            SearchText(
                textFieldValue = searchText,
                onTextChanged = { searchText = it },
            )
        }
        LazyColumn(
            modifier = Modifier.weight(1f).fillMaxWidth()
        ) {
            stickyHeader {
                Row(
                    modifier = Modifier
                        .background(MaterialTheme.colors.background)
                        .padding(
                            horizontal = 16.dp,
                            vertical = 4.dp
                        ),
                    horizontalArrangement = Arrangement.spacedBy(8.dp),
                    verticalAlignment = Alignment.CenterVertically,
                ) {
                    Text(
                        modifier = Modifier.weight(1f),
                        color = MaterialTheme.colors.onBackground,
                        text = "Идентификатор",
                    )
                    Text(
                        modifier = Modifier.weight(1f),
                        color = MaterialTheme.colors.onBackground,
                        text = "Расположение СК (БК)",
                    )
                    Text(
                        modifier = Modifier.weight(1f),
                        color = MaterialTheme.colors.onBackground,
                        text = "Расположение Р (М, ПМ)",
                    )
                    Text(
                        modifier = Modifier.weight(1f),
                        color = MaterialTheme.colors.onBackground,
                        text = "Класс безопасности",
                    )
                    Text(
                        modifier = Modifier.weight(1f),
                        color = MaterialTheme.colors.onBackground,
                        text = "Канал алгоритма",
                    )
                    Text(
                        modifier = Modifier.weight(1f),
                        color = MaterialTheme.colors.onBackground,
                        text = "Тип",
                    )
                    Text(
                        modifier = Modifier.weight(1f),
                        color = MaterialTheme.colors.onBackground,
                        text = "Питание",
                    )
                    Text(
                        modifier = Modifier.weight(1f),
                        color = MaterialTheme.colors.onBackground,
                        text = "Доп кабель",
                    )
                    Text(
                        modifier = Modifier.weight(1f),
                        color = MaterialTheme.colors.onBackground,
                        text = "Тип прибора СК (БК)",
                    )
                    Text(
                        modifier = Modifier.weight(1f),
                        color = MaterialTheme.colors.onBackground,
                        text = "Тип прибора Р (М, ПМ)",
                    )
                    Text(
                        modifier = Modifier.minimumInteractiveComponentSize(),
                        color = MaterialTheme.colors.onBackground,
                        text = "Delete",
                    )
                }
            }
            itemsIndexed(items,
                key = { _, item -> item[Lists.name] }
            ) { index, result ->
                TooltipArea(
                    modifier = Modifier
                        .animateItemPlacement()
                        .background(
                            if (index % 2 == 0) {
                                MaterialTheme.colors.surface.copy(alpha = 0.3f)
                            } else {
                                MaterialTheme.colors.background
                            }
                        )
                        .padding(
                            horizontal = 16.dp,
                            vertical = 8.dp
                        ),
                    tooltip = {
                        Text(
                            "Идентификатор: ${result[Lists.name]}\n" +
                                    "Расположение СК (БК): ${result[Premises.number]} - ${result[Premises.name]}\n" +
                                    "Расположение Р (М, ПМ): ${result[positionP[Premises.number]]} - ${result[positionP[Premises.name]]}\n" +
                                    "Класс безопасности: ${result[SecurityLevel.name]}\n" +
                                    "Канал алгоритма: ${result[AlgorithmChannel.name]} - ${result[AlgorithmChannel.channelCount]}\n" +
                                    "Тип: ${result[ListsType.name]}\n" +
                                    "Борт: ${result[Lists.board]}\n" +
                                    "Питание: ${result[Lists.energy]}\n" +
                                    "Участие в АЗ: ${result[Lists.participation] != null}\n" +
                                    "Доп кабель: ${result[Lists.additionalCable]}\n" +
                                    "Вторичный преобразователь: ${result[SecondaryConverter.name]}\n" +
                                    "Тип прибора СК (БК): ${result[Devices.name]}\n" +
                                    "Тип прибора Р (М, ПМ): ${result[devicesP[Devices.name]]}\n" +
                                    "Тип механизма: ${result[TypicalMechanisms.name]}\n" +
                                    "Имя сигнала: ${result[TypicalMechanismsSignalName.name]}\n" +
                                    "Тип сигнала (AI AO DO DI): ${result[TypicalMechanismsSignalType.name]}",
                            modifier = Modifier
                                .clip(MaterialTheme.shapes.medium)
                                .background(MaterialTheme.colors.surface)
                                .padding(16.dp),
                            color = MaterialTheme.colors.onBackground
                        )
                    },
                    content = {
                        Row(
                            horizontalArrangement = Arrangement.spacedBy(8.dp),
                            verticalAlignment = Alignment.CenterVertically,
                        ) {

                            //Идентификатор
                            Text(
                                modifier = Modifier.weight(1f),
                                color = MaterialTheme.colors.onBackground,
                                text = result[Lists.name].toString(),
                            )
                            //Расположение СК (БК)
                            Text(
                                modifier = Modifier.weight(1f),
                                color = MaterialTheme.colors.onBackground,
                                text = result[Premises.number].toString(),
                            )
                            //Расположение Р (М, ПМ)
                            Text(
                                modifier = Modifier.weight(1f),
                                color = MaterialTheme.colors.onBackground,
                                text = result[positionP[Premises.number]].toString(),
                            )
                            //Класс безопасности
                            Text(
                                modifier = Modifier.weight(1f),
                                color = MaterialTheme.colors.onBackground,
                                text = result[SecurityLevel.name].toString(),
                            )
                            //Канал алгоритма
                            Text(
                                modifier = Modifier.weight(1f),
                                color = MaterialTheme.colors.onBackground,
                                text = result[AlgorithmChannel.name].toString(),
                            )
                            //Тип
                            Text(
                                modifier = Modifier.weight(1f),
                                color = MaterialTheme.colors.onBackground,
                                text = result[ListsType.name].toString(),
                            )
                            //Питание
                            Box(
                                modifier = Modifier.weight(1f),
                                contentAlignment = Alignment.CenterStart
                            ) {
                                Checkbox(
                                    modifier = Modifier.fillMaxHeight(),
                                    checked = result[Lists.energy],
                                    onCheckedChange = {},
                                    enabled = false
                                )
                            }
                            //Доп кабель
                            Box(
                                modifier = Modifier.weight(1f),
                                contentAlignment = Alignment.CenterStart
                            ) {
                                Checkbox(
                                    modifier = Modifier.fillMaxHeight(),
                                    checked = result[Lists.additionalCable],
                                    onCheckedChange = {},
                                    enabled = false
                                )
                            }
                            //Тип прибора СК (БК)
                            Text(
                                modifier = Modifier.weight(1f),
                                color = MaterialTheme.colors.onBackground,
                                text = result[Devices.name].toString(),
                            )
                            //Тип прибора Р (М, ПМ)
                            Text(
                                modifier = Modifier.weight(1f),
                                color = MaterialTheme.colors.onBackground,
                                text = result[devicesP[Devices.name]].toString(),
                            )
                            IconButton(onClick = {
                                scope.launch(Dispatchers.IO) {
                                    runCatching {
                                        transaction {
                                            Lists.deleteWhere {
                                                name eq result[name]
                                            }
                                        }
                                    }.onFailure {
                                        sideEffect.emit(it.localizedMessage)
                                    }.onSuccess {
                                        changes++
                                    }
                                }
                            }) {
                                Icon(
                                    painter = painterResource("exit.svg"),
                                    contentDescription = "Delete",
                                    tint = MaterialTheme.colors.error,
                                )
                            }
                        }

                    },
                )
            }
        }
        Button(
            modifier = Modifier
                .padding(bottom = 16.dp)
                .padding(horizontal = 16.dp)
                .fillMaxWidth()
                .height(48.dp),
            enabled = tableCreated,
            contentPadding = PaddingValues(horizontal = 16.dp),
            onClick = {
                currentScreen.value = ListsAddScreen
            },
        ) {
            Text(
                text = "Добавить оборудование", color = MaterialTheme.colors.onPrimary
            )
        }
    }
}

