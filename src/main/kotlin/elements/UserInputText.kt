package elements

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.material.Icon
import androidx.compose.material.LocalTextStyle
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.focus.onFocusChanged
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.SolidColor
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp

@Composable
fun UserInputText(
    modifier: Modifier = Modifier,
    header: String,
    textFieldValue: String,
    onTextChanged: (String) -> Unit,
) {
    var hasFocus by remember { mutableStateOf(false) }
    Column(
        modifier = modifier,
    ) {
        Text(
            text = header,
            color = MaterialTheme.colors.onBackground,
            style = MaterialTheme.typography.subtitle1,
            maxLines = 1,
            overflow = TextOverflow.Ellipsis
        )
        BasicTextField(
            value = textFieldValue,
            onValueChange = { onTextChanged(it) },
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 8.dp)
                .height(48.dp)
                .onFocusChanged {
                    hasFocus = it.hasFocus
                },
            maxLines = 1,
            cursorBrush = SolidColor(MaterialTheme.colors.primaryVariant),
            textStyle = LocalTextStyle.current.copy(
                color = MaterialTheme.colors.onSurface
            ),
            decorationBox = {
                Box(
                    modifier = Modifier.clip(MaterialTheme.shapes.medium)
                        .background(MaterialTheme.colors.surface)
                        .border(
                            width = 1.dp,
                            color = if (hasFocus) MaterialTheme.colors.primaryVariant else Color.Transparent,
                            shape = MaterialTheme.shapes.medium
                        )
                        .padding(horizontal = 16.dp),
                    contentAlignment = Alignment.CenterStart,
                ) {
                    it.invoke()
                }
            }
        )
    }
}


@Composable
fun SearchText(
    modifier: Modifier = Modifier,
    textFieldValue: String,
    onTextChanged: (String) -> Unit,
) {
    var hasFocus by remember { mutableStateOf(false) }
    BasicTextField(
        value = textFieldValue,
        onValueChange = { onTextChanged(it) },
        modifier = modifier
            .widthIn(min = 80.dp)
            .height(48.dp)
            .onFocusChanged {
                hasFocus = it.hasFocus
            },
        maxLines = 1,
        cursorBrush = SolidColor(MaterialTheme.colors.primaryVariant),
        textStyle = LocalTextStyle.current.copy(
            color = MaterialTheme.colors.onSurface
        ),
        decorationBox = {
            Box(
                modifier = Modifier.clip(MaterialTheme.shapes.medium)
                    .background(MaterialTheme.colors.surface)
                    .border(
                        width = 1.dp,
                        color = if (hasFocus) MaterialTheme.colors.primaryVariant else MaterialTheme.colors.surface,
                        shape = MaterialTheme.shapes.medium
                    )
                    .padding(horizontal = 16.dp),
                contentAlignment = Alignment.CenterStart,
            ) {
                it.invoke()
                if (!hasFocus) {
                    Icon(
                        painter = painterResource("search.svg"),
                        contentDescription = "Search",
                        tint = MaterialTheme.colors.onBackground,
                        modifier = Modifier.align(Alignment.CenterEnd)
                    )
                }
            }
        }
    )
}

