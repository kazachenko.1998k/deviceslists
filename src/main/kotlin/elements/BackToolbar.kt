package elements

import Screen
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import currentScreen
import sceens.HomeScreen

@Composable
fun BackToolbar(
    header: String,
    backScreen: Screen = HomeScreen,
    endContent: @Composable () -> Unit = {}
) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .height(56.dp)
            .background(MaterialTheme.colors.surface),
        verticalAlignment = Alignment.CenterVertically
    ) {
        IconButton(
            onClick = { currentScreen.value = backScreen },
            modifier = Modifier.fillMaxHeight(),
            content = {
                Icon(
                    painter = painterResource("arrow_back.svg"),
                    contentDescription = "Back",
                    tint = MaterialTheme.colors.onSurface,
                )
            }
        )
        Text(
            text = header,
            style = MaterialTheme.typography.subtitle1,
            color = MaterialTheme.colors.onSurface,
            maxLines = 1,
            overflow = TextOverflow.Ellipsis
        )
        Spacer(Modifier.weight(1f))
        endContent.invoke()
    }
}