package elements

import Screen
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import currentScreen

@Composable
fun NavigationButton(modifier: Modifier, header: String, screen: Screen) {
    Button(
        modifier = modifier,
        contentPadding = PaddingValues(horizontal = 16.dp),
        onClick = {
            currentScreen.value = screen
        },
    ) {
        Text(
            text = header,
            color = MaterialTheme.colors.onPrimary
        )
    }
}
