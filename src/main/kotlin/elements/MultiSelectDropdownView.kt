package elements

import androidx.compose.animation.animateContentSize
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.focus.onFocusChanged
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.SolidColor
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import model.SingleNameTable
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.transaction
import sideEffect


@Composable
fun MultiSelectDropdownView(
    modifier: Modifier = Modifier,
    header: String,
    table: SingleNameTable,
    tableCreated: Boolean,
    selected: List<String>,
    onSelectNew: (String) -> Unit,
    onDeselectNew: (String) -> Unit,
    onDelete: () -> Unit,
) {
    var expanded by remember { mutableStateOf(false) }
    var items by remember { mutableStateOf(emptyList<String>()) }
    var newItemText by remember { mutableStateOf("") }
    var changes by remember { mutableStateOf(0) }
    val scope = rememberCoroutineScope()
    LaunchedEffect(changes, tableCreated) {
        if (tableCreated) {
            withContext(Dispatchers.IO) {
                transaction {
                    items = table.selectAll().map { it[table.name] }
                }
            }
        }
    }
    Box(modifier = modifier) {
        Column(
            modifier = modifier,
        ) {
            Text(
                text = header,
                color = MaterialTheme.colors.onBackground,
                style = MaterialTheme.typography.subtitle1,
                maxLines = 1,
                overflow = TextOverflow.Ellipsis,
            )
            Box(
                modifier = Modifier.padding(top = 8.dp).fillMaxWidth().height(48.dp)
                    .clickable(onClick = { expanded = true }).clip(MaterialTheme.shapes.medium)
                    .background(MaterialTheme.colors.primary).padding(horizontal = 16.dp),
            ) {
                Text(
                    text = selected.joinToString().ifEmpty { "Выберите" },
                    modifier = Modifier.align(Alignment.CenterStart),
                    color = MaterialTheme.colors.onPrimary
                )
            }
        }
        DropdownMenu(
            expanded = expanded,
            onDismissRequest = { expanded = false },
            modifier = Modifier.clip(MaterialTheme.shapes.large)
        ) {
            Column(
                modifier = Modifier.animateContentSize()
            ) {
                items.forEach { s ->
                    val checked = s in selected
                    DropdownMenuItem(
                        onClick = {
                            if (checked) {
                                onDeselectNew(s)
                            } else {
                                onSelectNew(s)
                            }
                        },
                    ) {
                        Text(
                            text = s,
                            modifier = Modifier.weight(1f)
                        )
                        Checkbox(
                            s in selected,
                            onCheckedChange = {
                                if (checked) {
                                    onDeselectNew(s)
                                } else {
                                    onSelectNew(s)
                                }
                            }
                        )
                        IconButton(onClick = {
                            scope.launch(Dispatchers.IO) {
                                runCatching {
                                    transaction {
                                        table.deleteWhere {
                                            table.name eq s
                                        }
                                    }
                                }.onFailure {
                                    sideEffect.emit(it.localizedMessage)
                                }.onSuccess {
                                    onDelete()
                                    onDeselectNew(s)
                                    changes++
                                }
                            }
                        }) {
                            Icon(
                                painter = painterResource("exit.svg"),
                                contentDescription = "Delete",
                                tint = MaterialTheme.colors.error,
                            )
                        }
                    }
                }
            }
            Row(
                modifier = Modifier.fillMaxWidth()
                    .padding(horizontal = 16.dp)
                    .padding(bottom = 8.dp)
                    .height(48.dp)
            ) {
                var hasFocus by remember { mutableStateOf(false) }
                BasicTextField(value = newItemText,
                    onValueChange = { newItemText = it },
                    modifier = Modifier.weight(1f).fillMaxHeight().onFocusChanged {
                        hasFocus = it.hasFocus
                    },
                    maxLines = 1,
                    cursorBrush = SolidColor(MaterialTheme.colors.primaryVariant),
                    textStyle = LocalTextStyle.current.copy(
                        color = MaterialTheme.colors.onSurface
                    ),
                    decorationBox = {
                        Box(
                            modifier = Modifier.clip(MaterialTheme.shapes.medium)
                                .background(MaterialTheme.colors.surface).border(
                                    width = 1.dp,
                                    color = if (hasFocus) MaterialTheme.colors.primaryVariant else Color.Transparent,
                                    shape = MaterialTheme.shapes.medium
                                ).padding(horizontal = 16.dp),
                            contentAlignment = Alignment.CenterStart,
                        ) {

                            Text(
                                "Write new value", color = MaterialTheme.colors.onSurface.copy(
                                    alpha = if (newItemText.isEmpty()) 0.3f else 0f
                                )
                            )
                            it.invoke()
                        }
                    })
                Button(
                    modifier = Modifier.fillMaxHeight().padding(start = 8.dp),
                    enabled = newItemText.isNotEmpty(),
                    contentPadding = PaddingValues(horizontal = 16.dp),
                    onClick = {
                        scope.launch(Dispatchers.IO) {
                            transaction {
                                runCatching {
                                    table.insert {
                                        it[name] = newItemText
                                    }
                                }
                            }.onFailure {
                                sideEffect.emit(it.localizedMessage)
                            }.onSuccess {
                                newItemText = ""
                                changes++
                            }
                        }
                    },
                ) {
                    Text(
                        text = "Add", color = MaterialTheme.colors.onPrimary
                    )
                }

            }
        }
    }
}