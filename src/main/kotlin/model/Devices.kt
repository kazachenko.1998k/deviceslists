package model

import org.jetbrains.exposed.sql.ReferenceOption
import org.jetbrains.exposed.sql.Table

object Devices : SingleNameTable() {
    val aiField = optReference("ai_field_id", AIField.id, onDelete = ReferenceOption.SET_NULL)
    val aoField = optReference("ao_field_id", AOField.id, onDelete = ReferenceOption.SET_NULL)
    val diField = optReference("di_field_id", DIField.id, onDelete = ReferenceOption.SET_NULL)
    val doField = optReference("do_field_id", DOField.id, onDelete = ReferenceOption.SET_NULL)
    val relayField = optReference("relay_field_id", RelayField.id, onDelete = ReferenceOption.SET_NULL)
}

data class DeviceModel(
    val name: String,
    val aiField: String?,
    val aoField: String?,
    val diField: String?,
    val doField: String?,
    val relayField: String?,
    val terminals: List<String>,
)

object AIField : SingleNameTable()

object AOField : SingleNameTable()

object DIField : SingleNameTable()

object DOField : SingleNameTable()
object TerminalField : SingleNameTable()

object RelayField : SingleNameTable()

object TerminalsToDevicesField : Table() {
    val terminal = reference("terminal_id", TerminalField.id, onDelete = ReferenceOption.CASCADE)
    val device = reference("device_id", Devices.id, onDelete = ReferenceOption.CASCADE)
    override val primaryKey = PrimaryKey(terminal, device)
}
