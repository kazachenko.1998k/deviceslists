package model

import org.jetbrains.exposed.sql.Column
import org.jetbrains.exposed.sql.Table

object Lists : Table(), NameTable {
    override val name: Column<String> = varchar("name", 64).uniqueIndex()
    val positionCK = reference("position_ck", Premises.id)
    val positionP = reference("position_p", Premises.id)
    val securityLevel = reference("security_level", SecurityLevel.id)
    val algorithmChannel = reference("algorithm_channel", AlgorithmChannel.id)
    val type = reference("type", ListsType.id)
    val board = integer("board")
    val energy = bool("energy")
    val participation = optReference("participation", Participation.id)
    val additionalCable = bool("additional_cable")
    val secondaryConverter = reference("secondary_converter", SecondaryConverter.id)
    val deviceCK = reference("device_ck", Devices.id)
    val deviceP = reference("device_p", Devices.id)
    val mechanismType = reference("mechanism_type", TypicalMechanisms.name)
    override val primaryKey = PrimaryKey(name)
}

object SecurityLevel : SingleNameTable()

object ListsType : SingleNameTable()

object SecondaryConverter : SingleNameTable()
