package model

object Premises : SingleNameTable() {
    val number = varchar("number", 64).uniqueIndex()
    val canUsedForDevice = bool("can_used_for_device")
}