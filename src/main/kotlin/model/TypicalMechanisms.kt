package model

import org.jetbrains.exposed.sql.Column
import org.jetbrains.exposed.sql.Table

object TypicalMechanisms : Table(), NameTable {
    override val name: Column<String> = varchar("name", 64).uniqueIndex()
    val inputCount = integer("input_count")
    val outputCount = integer("output_count")
    val signalName = reference("signal_name", TypicalMechanismsSignalName.id)
    val signalType = reference("signal_type", TypicalMechanismsSignalType.id)
    override val primaryKey = PrimaryKey(name)
}

object TypicalMechanismsSignalName : SingleNameTable()

object TypicalMechanismsSignalType : SingleNameTable()
