package model

import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.Column

sealed class SingleNameTable : IntIdTable(), NameTable {
    override val name: Column<String> = varchar("name", 64).uniqueIndex()
}

sealed interface NameTable {
    val name: Column<String>
}