package model

object AlgorithmChannel : SingleNameTable(), NameTable {
    val channelCount = integer("channel_count")
}