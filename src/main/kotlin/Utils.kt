import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.exists


fun createTablesIfNotExists(vararg tables: Table) {
    tables.filter { !it.exists() }.let {
        if (it.isNotEmpty()) {
            SchemaUtils.create(*it.toTypedArray())
        }
    }
}